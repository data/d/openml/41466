# OpenML dataset: enron

https://www.openml.org/d/41466

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multi-label dataset. The UC Berkeley enron4 dataset represents a
  subset of the original enron5 dataset and consists of 1684 cases of emails
  with 21 labels and 1001 predictor variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41466) of an [OpenML dataset](https://www.openml.org/d/41466). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41466/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41466/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41466/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

